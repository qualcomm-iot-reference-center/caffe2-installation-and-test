import numpy as np
import caffe2
from caffe2.proto import caffe2_pb2
from caffe2.python import workspace
import cv2

def crop_center(img,cropx,cropy):
    y,x,c = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)    
    return img[starty:starty+cropy,startx:startx+cropx]

def rescale(img, input_height, input_width):
    print("Original image shape:" + str(img.shape) + " and remember it should be in H, W, C!")
    aspect = img.shape[1]/float(img.shape[0])
    print("Orginal aspect ratio: " + str(aspect))
    if(aspect>1):
        # landscape orientation - wide image
        res = int(aspect * input_height)
        imgScaled = cv2.resize(img, (res,input_width))
    if(aspect<1):
        # portrait orientation - tall image
        res = int(input_width/aspect)
        imgScaled = cv2.resize(img, (input_height,res))
    if(aspect == 1):
        imgScaled = cv2.resize(img, (input_height, input_width))
    print("New image shape:" + str(imgScaled.shape) + " in HWC")
    return imgScaled

def main():
	IMAGE_LOCATION="flower.jpg"
	mean = 128
	INPUT_IMAGE_SIZE=227

    # load and transform image
	img = cv2.imread(IMAGE_LOCATION).astype(np.float32)
	img = img / 255
	
	#transform image: rescale / crop / color space
	img = rescale(img, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE)
	img = crop_center(img, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE)
	img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

	print ("After crop: " , img.shape)
	img = img.swapaxes(1, 2).swapaxes(0, 1)
	
	# switch to BGR
	img = img[(2, 1, 0), :, :]
	
	# remove mean for better results
	img = img * 255 - mean
	
	# add batch size
	img = img[np.newaxis, :, :, :].astype(np.float32)
	print ("NCHW: ", img.shape)

	with open('squeezenet/init_net.pb', 'rb') as f:
		init_net = f.read()
	with open('squeezenet/predict_net.pb', 'rb') as f:
		predict_net = f.read()

	predictor = caffe2.python.workspace.Predictor(init_net, predict_net)
	
	output = predictor.run({"data": img})
	results = np.asarray(output)
	print ("results shape: ", results.shape)

	results = np.delete(results, 1)
	index = 0
	highest = 0
	arr = np.empty((0,2), dtype=object)
	arr[:,0] = int(10)
	arr[:,1:] = float(10)
	for i, r in enumerate(results):
		# imagenet index begins with 1!
		i=i+1
		arr = np.append(arr, np.array([[i,r]]), axis=0)
		if (r > highest):
			highest = r
			index = i

	print (index, " :: ", highest)

if __name__ == '__main__':
	workspace.GlobalInit(['caffe2', '--caffe2_log_level=2'])
	main()
