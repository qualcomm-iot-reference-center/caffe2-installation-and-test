# **Instalação Caffe2 na DragonBoard 410C**


Este artigo apresenta os procedimentos necessários para embarcar o Caffe2 na plataforma Qualcomm® Snapdragon™ 410E.

Caffe2 é um framework para desenvolver algoritmos de deep learning. Foi desenvolvido com propósito de disponibilizar uma ferramenta leve voltado para uso de produção. Isto é, para embarcar recursos de IA em produtos. Considerando isso, o framework tem suporte para desenvolvimento em múltiplas plataformas, tais como: Linux, Mac, iOS, Android e Windows. 

#### **Hardware Utilizado**

> Essa aplicação foi executada na plataforma de desenvolvimento DragonBoard410c. A imagem do sistema operacional é a linaro-buster-alip-dragonboard-410c-359.img.gz e pode ser obtida neste link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/ 

> Versão do kernel: 

> Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux 

> Cabe ressaltar que o sistema utiliza um uma memória externa para expandir a capacidade do rootfs e fornecer uma região de swap. A configuração desse mecanismo é a seguinte: de 4GB de swap e o restante dedicado para o rootfs. 


## **Instalação do Caffe2 na Dragonboard410C**

Atualmente, o Caffe2 e mantido junto ao PyTorch no repositório do git: 

 https://github.com/pytorch/pytorch/. 
 
 O procedimento de instalação do framework na DB410C é detalhado a seguir. Cabe ressaltar que o procedimento é dependente da versão 2.7 do Python.
 Para seguir a sequência de instalação, inicie um terminal com o usuário desejado.
 
 

#### **Primeiro passo**

Antes de executar o script de compilação, faz-se necessário criar uma região de swap devido à quantidade de memória exigida para o procedimento de compilação. 
Tal procedimento pode ser realizado conforme indicado [neste tutorial](https://www.96boards.org/documentation/consumer/guides/zram_swapspace.md.html). Assim sendo, crie um script zram.sh no diretório do usuário. 

```sh
cd /home/$USER
sudo nano zram.sh 
```

O conteúdo do *script* é mostrado abaixo.

```sh
#!/bin/bash 
modprobe zram num_devices=4 

totalmem=`free | grep -e "^Mem:" | awk '{print $2}'` 
mem=$(( ($totalmem)* 1024 * 3)) 

echo $mem > /sys/block/zram0/disksize 

mkswap /dev/zram0 

swapon -p 5 /dev/zram0
```

Em seguida, execute o script zram.sh. 

```sh
sudo chmod +x zram.sh 
bash zram.sh
```

> Se o comando falhar, execute com sudo: sudo bash zram.sh

Outra maneira de realizar tal procedimento é descrita [neste tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md).
 
#### **Segundo passo** 
 
 A compilação deve utilizar a [versão 3.10 do cmake](https://www.claudiokuenzler.com/blog/755/install-upgade-cmake-3.10.1-ubuntu-14.04-trusty-alternatives#.W_U4tnVKg5l). 

```sh
cd /home/$USER
wget http://www.cmake.org/files/v3.10/cmake-3.10.1.tar.gz 
tar -xvzf cmake-3.10.1.tar.gz 
cd cmake-3.10.1/ 
./configure 
make
make install
```

> Se o comando falhar, execute como super usuário: sudo make install


#### **Terceiro passo**

Realizar o download do repositório do pytorch na DB410C. 

```sh
cd /home/$USER 
git clone --recursive https://github.com/pytorch/pytorch.git 
cd pytorch 
git submodule update --init 
```

Adicionar procedimento de build no diretório scripts. Os comandos indicados nos passos a seguir devem ser executados no diretório pytorch. 

```sh
nano ./scripts/build_db410c.sh 
```

Em seguida, preencha o arquivo com o conteúdo indicado a seguir: 

```sh
#!/bin/bash
CAFFE2_ROOT="$( cd "$(dirname -- "$0")"/.. ; pwd -P)"
BUILD_ROOT=${BUILD_ROOT:-"$CAFFE2_ROOT/build"} 
mkdir -p $BUILD_ROOT
echo "Installing dependencies."
apt update
sudo apt install -y python-pip cmake libgflags-dev libgoogle-glog-dev libprotobuf-dev libpython-dev python-pip python-numpy protobuf-compiler
echo "Installing python dependencies."
sudo pip install hypothesis
sudo pip install typing
sudo pip install protobuf -U
sudo pip install future
sudo pip install pyyaml
echo "Building caffe2"
cd $BUILD_ROOT
cmake -j 2 "$CAFFE2_ROOT" -DCMAKE_VERBOSE_MAKEFILE=1 -DCAFFE2_CPU_FLAGS="-mfpu=neon -mfloat-abi=hard" -DLMDB=ON || exit 1
make -j 2 || exit 1
```

> Cabe ressaltar que o parâmetro “–j 2” para make e cmake deve ser usado para evitar o alto consumo de RAM durante a compilação. 

 

#### **Quarto passo**

No diretório do pytorch, execute o *script build_db410c.sh*. 

```sh
chmod u+x ./scripts/build_db410c.sh 
./scripts/build_db410c.sh 
```

Por fim, inicie o procedimento de instalação. 

```sh
cd build 
make install
sudo ldconfig
```

#### **Quinto passo**

No terminal, abra o Python e teste a importação dos seguintes pacotes:

```python 
python
>> import caffe2 
>> from caffe2.proto import caffe2_pb2 
>> from caffe2.python import workspace 
```

Se ocorrer algum erro nesse ponto será devido alguma biblioteca pendente. Após a inclusão da biblioteca, somente a notificação abaixo deve aparecer no console. 


> WARNING:This caffe2 python run does not have GPU support. Will run in CPU only mode.


### **Aplicação de Exemplo**

Para demonstrar a aplicação de reconhecimento de imagens será apresentado o procedimento para utilização de modelos de Redes Convolucionais pré treinados. O [Model Zoo](https://caffe2.ai/docs/zoo.html) fornece uma série de aplicações com *datasets* extensos. Uma delas é a Squeezenet, uma rede para reconhecimento de imagens com 1000 objetos classificados. Para mais informações, consulte o seguinte link: https://caffe2.ai/docs/tutorial-loading-pre-trained-models.html.


Para tal, considere a [imagem](https://pixabay.com/p-631765/?no_redirect) abaixo:

 <div align="center">
    <figure>
        <img src="/Tutorial/flower.jpg">
        <figcaption>Fonte:pixabay.</figcaption>
    </figure>
</div> 


O resultado da execução do *script* é o seguinte:

> Original image shape:(751, 1280, 3) and remember it should be in H, W, C!        
> Model's input shape is 227x227                       
> Orginal aspect ratio: 1.70439414115                  
> New image shape:(227, 386, 3) in HWC                 
> After crop:  (227, 227, 3)                           
> NCHW:  (1, 3, 227, 227)                              
> results shape:  (1, 1, 1000, 1, 1)                   
> 985  ::  0.9790583  

Na ultimá linha, o ID 985 corresponde ao objeto *daisy*. A lista de objetos classificados pode ser vista neste [link](https://gits.githubusercontent.com/aaronmarkham/cd3a6b6ac071eca6f7b4a6e40e6038aa/raw/9edb4038a37da6b5a44c3b5bc52e448ff09bfe5b/alexnet_codes). 



Antes de excutar, certifique-se de que os pacotes numpy e cv2 estão instalados:

```sh
sudo apt-get install python-opencv
sudo pip install numpy
```

O *script* completo é mostrado abaixo pode ser obtido no repositório.

```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test.git 
```

Antes de testá-lo, cabe destacar os principais procedimentos realizados: 

- Primeiro, os modelos [init_net.pb](https://s3.amazonaws.com/download.caffe2.ai/models/squeezenet/init_net.pb) e [predict_net.pb](https://s3.amazonaws.com/download.caffe2.ai/models/squeezenet/predict_net.pb) são carregados para o ambiente. 
```python
with open('squeezenet/init_net.pb', 'rb') as f: 
   init_net = f.read() 
with open('squeezenet/predict_net.pb', 'rb') as f: 
   predict_net = f.read() 
```

- Em seguida, a rede é inicializada com os modelos indicados. 
```python
predictor = caffe2.python.workspace.Predictor(init_net, predict_net) 
```
- Neste ponto, basta inserir uma imagem na entrada da rede e executar o procedimento de reconhecimento: 
```python
output = predictor.run({"data": img}) 
```

A classificação será retornada no variável *output*. Para executar o código:

```sh
cd caffe2-installation-and-test
python test.py
```


```python
import numpy as np 
import caffe2 
from caffe2.proto import caffe2_pb2 
from caffe2.python import workspace 
import cv2 


def crop_center(img,cropx,cropy): 
   y,x,c = img.shape 
   startx = x//2-(cropx//2) 
   starty = y//2-(cropy//2)     
   return img[starty:starty+cropy,startx:startx+cropx] 

def rescale(img, input_height, input_width): 
   print("Original image shape:" + str(img.shape) + " and remember it should be in H, W, C!") 
   aspect = img.shape[1]/float(img.shape[0]) 
   print("Orginal aspect ratio: " + str(aspect)) 
   if(aspect>1): 
       # landscape orientation - wide image 
       res = int(aspect * input_height) 
       imgScaled = cv2.resize(img, (res,input_width)) 
   if(aspect<1): 
       # portrait orientation - tall image 
       res = int(input_width/aspect) 
       imgScaled = cv2.resize(img, (input_height,res)) 
   if(aspect == 1): 
       imgScaled = cv2.resize(img, (input_height, input_width)) 
   print("New image shape:" + str(imgScaled.shape) + " in HWC") 
   return imgScaled 

  
def main(): 
    IMAGE_LOCATION="flower.jpg" 
    
    mean = 128 
    INPUT_IMAGE_SIZE=227 
    
       # load and transform image 
    img = cv2.imread(IMAGE_LOCATION).astype(np.float32) 
    img = img / 255 
     
    
    #transform image: rescale / crop / color space 
    img = rescale(img, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE) 
    img = crop_center(img, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE) 
    img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB) 
    
      
    print ("After crop: " , img.shape) 
    img = img.swapaxes(1, 2).swapaxes(0, 1) 
    
     
    # switch to BGR 
    img = img[(2, 1, 0), :, :] 
    
     
    # remove mean for better results 
    img = img * 255 - mean 
    
     
    # add batch size 
    img = img[np.newaxis, :, :, :].astype(np.float32) 
    print ("NCHW: ", img.shape) 
    
    
    with open('squeezenet/init_net.pb', 'rb') as f: 
        init_net = f.read() 
    with open('squeezenet/predict_net.pb', 'rb') as f: 
        predict_net = f.read() 
    
      
    predictor = caffe2.python.workspace.Predictor(init_net, predict_net) 
    
    output = predictor.run({"data": img}) 
    results = np.asarray(output) 
    print ("results shape: ", results.shape) 
    
      
    
    results = np.delete(results, 1) 
    index = 0 
    highest = 0 
    arr = np.empty((0,2), dtype=object) 
    arr[:,0] = int(10) 
    arr[:,1:] = float(10) 
    for i, r in enumerate(results): 
        # imagenet index begins with 1! 
        i=i+1 
        arr = np.append(arr, np.array([[i,r]]), axis=0) 
        if (r > highest): 
            highest = r 
            index = i 
    
      
    print (index, " :: ", highest) 


if __name__ == '__main__': 
    workspace.GlobalInit(['caffe2', '--caffe2_log_level=2']) 
    main() 
```

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
