# Installing Caffe2 on DragonBoard 410C


This article outlines the procedures required to ship Caffe2 on the Qualcomm® Snapdragon ™ 410E platform.

Caffe2 is a framework for developing deep learning algorithms. It was developed with the purpose of providing a light tool for production use. That is, to embed IA resources into products. For that, the framework has support for development in multiple platforms, such as Linux, Mac, iOS, Android and Windows.


#### Hardware Used

> This application was run on the DragonBoard410c development platform. The image of the operating system is linaro-buster-alip-dragonboard-410c-359.img.gz and can be obtained at this link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/

> Kernel version:

> Linux qubuntu 4.15.0-36-generic # 39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU / Linux

> It should be noted that the system uses an external memory to expand the capacity of the rootfs and provide a swap region. The configuration of this mechanism is as follows: 4GB swap and the rest dedicated to the rootfs.



## Installing Caffe2 on Dragonboard410C

Currently, Caffe2 is maintained by PyTorch in the git repository:

 https://github.com/pytorch/pytorch/.
 
 The procedure below details how to install the framework in DB410C. It should be noted that the procedure is dependent on version 2.7 of Python.
 To follow the installation sequence, start a terminal with the desired user.



#### First step

Before running the build script, it is necessary to create a swap region because of the amount of memory required for the build procedure.
This procedure can be performed as instructed [in this tutorial](https://www.96boards.org/documentation/consumer/guides/zram_swapspace.md.html). Therefore, create a zram.sh script in the user directory.

```sh
cd /home/$USER
sudo nano zram.sh 
```

The contents of *script* are shown below.


```sh
#!/bin/bash 
modprobe zram num_devices=4 

totalmem=`free | grep -e "^Mem:" | awk '{print $2}'` 
mem=$(( ($totalmem)* 1024 * 3)) 

echo $mem > /sys/block/zram0/disksize 

mkswap /dev/zram0 

swapon -p 5 /dev/zram0
```


Then run the zram.sh script.

```sh
sudo chmod +x zram.sh 
bash zram.sh
```

> If the command fails, run with sudo: sudo bash zram.sh

Another way to perform such a procedure is described [in this tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.EN.md).



#### Second step
 
 The build should use [version 3.10 of cmake](https://www.claudiokuenzler.com/blog/755/install-upgade-cmake-3.10.1-ubuntu-14.04-trusty-alternatives#.W_U4tnVKg5l).
 
 ```sh
cd /home/$USER
wget http://www.cmake.org/files/v3.10/cmake-3.10.1.tar.gz 
tar -xvzf cmake-3.10.1.tar.gz 
cd cmake-3.10.1/ 
./configure 
make
make install
```

> If the command fails, run with super user privileges: sudo make install


#### Third step

Download the pytorch repository on the DB410C.

```sh
cd /home/$USER 
git clone --recursive https://github.com/pytorch/pytorch.git 
cd pytorch 
git submodule update --init 
```


Add build procedure to scripts directory. The commands indicated in the following steps must be executed in the pytorch directory.

```sh
nano ./scripts/build_db410c.sh 
```


Then fill in the file with the following content:

```sh
#!/bin/bash
CAFFE2_ROOT="$( cd "$(dirname -- "$0")"/.. ; pwd -P)"
BUILD_ROOT=${BUILD_ROOT:-"$CAFFE2_ROOT/build"} 
mkdir -p $BUILD_ROOT
echo "Installing dependencies."
apt update
sudo apt install -y python-pip cmake libgflags-dev libgoogle-glog-dev libprotobuf-dev libpython-dev python-pip python-numpy protobuf-compiler
echo "Installing python dependencies."
sudo pip install hypothesis
sudo pip install typing
sudo pip install protobuf -U
sudo pip install future
sudo pip install pyyaml
echo "Building caffe2"
cd $BUILD_ROOT
cmake -j 2 "$CAFFE2_ROOT" -DCMAKE_VERBOSE_MAKEFILE=1 -DCAFFE2_CPU_FLAGS="-mfpu=neon -mfloat-abi=hard" -DLMDB=ON || exit 1
make -j 2 || exit 1
```

> It should be noted that the "-j 2" parameter for make and cmake should be used to avoid high RAM consumption during compilation.


#### Fourth step

In the pytorch directory, run the *script build_db410c.sh*.

```sh
chmod u+x ./scripts/build_db410c.sh 
./scripts/build_db410c.sh 
```


Finally, start the installation procedure.

```sh
cd build 
make install
sudo ldconfig
```


#### Step Five

At the terminal, open Python and test the import of the following packages:

```python 
python
>> import caffe2 
>> from caffe2.proto import caffe2_pb2 
>> from caffe2.python import workspace 
```

If any errors occur at this point, some pending library may be causing this. Adding the library, only the notification below should appear on the console.

> WARNING:This caffe2 python run does not have GPU support. Will run in CPU only mode.


### Example Application

For an image recognition application will be presented the procedure for using pre-trained Convolutional Networks models. [Model Zoo](https://caffe2.ai/docs/zoo.html) provides a series of applications with extensive datasets. One of them is a Squeezenet, a network for image recognition with 1000 classified objects. For more information, see the following link: https://caffe2.ai/docs/tutorial-loading-pre-trained-models.html.


To do this, consider the [image](https://pixabay.com/p-631765/?no_redirect) below:

 <div align="center">
    <figure>
        <img src="/Tutorial/flower.jpg">
        <figcaption>Source: pixabay.</figcaption>
    </figure>
</div>

The result of running script is as follows:

> Original image shape:(751, 1280, 3) and remember it should be in H, W, C!        
> Model's input shape is 227x227                       
> Orginal aspect ratio: 1.70439414115                  
> New image shape:(227, 386, 3) in HWC                 
> After crop:  (227, 227, 3)                           
> NCHW:  (1, 3, 227, 227)                              
> results shape:  (1, 1, 1000, 1, 1)                   
> 985  ::  0.9790583


At the last line, ID 985 corresponds to the daisy object. The list of classified objects can be seen at this [link](https://gits.githubusercontent.com/aaronmarkham/cd3a6b6ac071eca6f7b4a6e40e6038aa/raw/9edb4038a37da6b5a44c3b5bc52e448ff09bfe5b/alexnet_codes).


Before you run, make sure that the numpy and cv2 packages are installed:

```sh
sudo apt-get install python-opencv
sudo pip install numpy
```


The complete script shown below can be obtained from the repository.

```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test.git 
```

Before testing it, it is important to highlight the main procedures performed:

- First, the [init_net.pb](https://s3.amazonaws.com/download.caffe2.ai/models/squeezenet/init_net.pb) and [predict_net.pb](https://s3.amazonaws.com/download.caffe2.ai/models/squeezenet/predict_net.pb) models are loaded into the environment.
- 
```python
with open('squeezenet/init_net.pb', 'rb') as f: 
   init_net = f.read() 
with open('squeezenet/predict_net.pb', 'rb') as f: 
   predict_net = f.read() 
```

- The network is then initialized with the indicated models. 
```python
predictor = caffe2.python.workspace.Predictor(init_net, predict_net) 
```
- At this point, simply insert an image at the input of the network and perform the recognition procedure: 
```python
output = predictor.run({"data": img}) 
```

The sort will be returned in the output variable. To run the code:

```sh
cd caffe2-installation-and-test
python test.py
```


```python
import numpy as np 
import caffe2 
from caffe2.proto import caffe2_pb2 
from caffe2.python import workspace 
import cv2 


def crop_center(img,cropx,cropy): 
   y,x,c = img.shape 
   startx = x//2-(cropx//2) 
   starty = y//2-(cropy//2)     
   return img[starty:starty+cropy,startx:startx+cropx] 

def rescale(img, input_height, input_width): 
   print("Original image shape:" + str(img.shape) + " and remember it should be in H, W, C!") 
   aspect = img.shape[1]/float(img.shape[0]) 
   print("Orginal aspect ratio: " + str(aspect)) 
   if(aspect>1): 
       # landscape orientation - wide image 
       res = int(aspect * input_height) 
       imgScaled = cv2.resize(img, (res,input_width)) 
   if(aspect<1): 
       # portrait orientation - tall image 
       res = int(input_width/aspect) 
       imgScaled = cv2.resize(img, (input_height,res)) 
   if(aspect == 1): 
       imgScaled = cv2.resize(img, (input_height, input_width)) 
   print("New image shape:" + str(imgScaled.shape) + " in HWC") 
   return imgScaled 

  
def main(): 
    IMAGE_LOCATION="flower.jpg" 
    
    mean = 128 
    INPUT_IMAGE_SIZE=227 
    
       # load and transform image 
    img = cv2.imread(IMAGE_LOCATION).astype(np.float32) 
    img = img / 255 
     
    
    #transform image: rescale / crop / color space 
    img = rescale(img, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE) 
    img = crop_center(img, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE) 
    img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB) 
    
      
    print ("After crop: " , img.shape) 
    img = img.swapaxes(1, 2).swapaxes(0, 1) 
    
     
    # switch to BGR 
    img = img[(2, 1, 0), :, :] 
    
     
    # remove mean for better results 
    img = img * 255 - mean 
    
     
    # add batch size 
    img = img[np.newaxis, :, :, :].astype(np.float32) 
    print ("NCHW: ", img.shape) 
    
    
    with open('squeezenet/init_net.pb', 'rb') as f: 
        init_net = f.read() 
    with open('squeezenet/predict_net.pb', 'rb') as f: 
        predict_net = f.read() 
    
      
    predictor = caffe2.python.workspace.Predictor(init_net, predict_net) 
    
    output = predictor.run({"data": img}) 
    results = np.asarray(output) 
    print ("results shape: ", results.shape) 
    
      
    
    results = np.delete(results, 1) 
    index = 0 
    highest = 0 
    arr = np.empty((0,2), dtype=object) 
    arr[:,0] = int(10) 
    arr[:,1:] = float(10) 
    for i, r in enumerate(results): 
        # imagenet index begins with 1! 
        i=i+1 
        arr = np.append(arr, np.array([[i,r]]), axis=0) 
        if (r > highest): 
            highest = r 
            index = i 
    
      
    print (index, " :: ", highest) 


if __name__ == '__main__': 
    workspace.GlobalInit(['caffe2', '--caffe2_log_level=2']) 
    main() 
```